import {ApplicationRef, enableProdMode, NgModuleRef} from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {enableDebugTools} from '@angular/platform-browser';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .then(appModuleRef => {
    const appRef = appModuleRef.injector.get(ApplicationRef);
    const rootComponent = appRef.components[0];
    enableDebugTools(rootComponent);
  })
  .catch(err => console.log(err));
