import { Component, OnInit } from '@angular/core';

interface Order {
  ref: number;
  date: Date;
  price: number;
  status: 'cancel'|'pending'|'paid'|'delivered';
}

@Component({
  selector: 'mp-account-orders',
  templateUrl: './account-orders.component.html'
})
export class AccountOrdersComponent {

  orders: Array<Order> = [
    {ref: 1, date: new Date(1515945682000), price: 1225, status: 'delivered'},
    {ref: 2, date: new Date(1516204882000), price: 5940, status: 'paid'},
    {ref: 3, date: new Date(1517396482000), price: 1579, status: 'pending'}
  ];
}
