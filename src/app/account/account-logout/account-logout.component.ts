import {Component, OnInit} from '@angular/core';
import {SecurityService} from '../../core/security/security.service';
import {Router} from '@angular/router';

@Component({
  selector: 'mp-account-logout',
  template: '<div class="text-center">Logout from app ...</div>'
})
export class AccountLogoutComponent implements OnInit {

  constructor(private securityService: SecurityService,
              private routerService: Router) {
  }

  ngOnInit() {
    this.securityService.logout();
    this.routerService.navigate(['/']);
  }
}
