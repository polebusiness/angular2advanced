import {Component, OnInit} from '@angular/core';
import {User} from '../shared/user.model';
import {SecurityService} from '../../core/security/security.service';
import {UserService} from '../shared/user.service';

@Component({
  selector: 'mp-account-infos',
  templateUrl: './account-infos.component.html'
})
export class AccountInfosComponent implements OnInit {

  user: User;

  constructor(private securityService: SecurityService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.userService.current()
      .subscribe((user) => {
        this.user = user;
      });
  }

  simulateUnAuthorised() {
    this.userService.unAuthorised().subscribe();
  }
}
