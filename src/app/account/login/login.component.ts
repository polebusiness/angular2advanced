import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../shared/user.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {SecurityService} from '../../core/security/security.service';

@Component({
  selector: 'mp-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  @ViewChild(NgForm) loginForm: NgForm;

  authError = false;

  credentials: { email: string, password: string } = {email: null, password: null};

  constructor(private userService: UserService,
              private routerService: Router,
              private securityService: SecurityService) {
  }

  ngOnInit() {
    this.loginForm.valueChanges.subscribe(() => {
      this.authError = false;
    });
  }

  authenticate(): void {
    this.userService
      .authenticate(this.credentials)
      .subscribe(
        (token) => this.login(token),
        () => this.authError = true
      );
  }

  private login(token: string) {
    this.securityService
      .login(token, this.userService.current())
      .subscribe((user) => {
        this.routerService.navigate(['/account']);
      });
  }
}
