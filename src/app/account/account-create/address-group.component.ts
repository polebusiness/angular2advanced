import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Country, COUNTRY_LIST} from '../shared/user.model';

@Component({
  selector: 'mp-address-group',
  templateUrl: 'address-group.component.html'
})
export class AddressGroupComponent {
  @Input() group: FormGroup;
  @Input() isAddressDelivery: boolean;
  @Output() defaultAddressChange: EventEmitter<boolean> = new EventEmitter();
  countryList: Array<Country> = COUNTRY_LIST;

  onDefaultAddressChange(status: boolean): void {
    this.defaultAddressChange.emit(status);
  }
}
