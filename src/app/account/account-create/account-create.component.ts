import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User, GENDER_LIST, COUNTRY_LIST, Gender, AddressDelivery, Country, Address} from '../shared/user.model';
import {UserService} from '../shared/user.service';

@Component({
  selector: 'mp-account-create',
  templateUrl: './account-create.component.html'
})
export class AccountCreateComponent implements OnInit {
  user: User;
  genderList: Array<Gender> = GENDER_LIST;
  accountCreateForm: FormGroup;

  constructor(public router: Router,
              private userService: UserService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initUser();
    this.accountCreateForm = this.fb.group({
      gender: [this.user.gender, [Validators.required]],
      firstName: [this.user.firstName, [Validators.required]],
      lastName: [this.user.lastName, [Validators.required]],
      email: [this.user.email, [Validators.required, Validators.email]],
      addressInvoice: this.createAddressGroup(new Address()),
      addressesDelivery: this.fb.array([])
    });
  }

  private createAddressGroup(address: Address): FormGroup {

    const addressGroup = this.fb.group({
      street: [address.street, Validators.required],
      city: [address.city, Validators.required],
      zipCode: [address.zipCode, Validators.required],
      country: [address.country, Validators.required],
    });

    if (address instanceof AddressDelivery) {
      addressGroup.addControl('isDefault', new FormControl());
    }

    return addressGroup;
  }

  initUser(): void {
    this.user = new User();
    this.user.gender = 'Mr';
  }

  save(): void {
    if (this.accountCreateForm.valid) {
      this.user = this.accountCreateForm.getRawValue() as User;
      this.userService
        .create(this.user)
        .subscribe(() => this.router.navigate(['/account']));
    }
  }

  getAddressDeliveryArray(): FormArray {
    return this.accountCreateForm.get('addressesDelivery') as FormArray;
  }

  addDeliveryAddress(): void {
    const addressesDeliveryGroup = this.getAddressDeliveryArray();
    addressesDeliveryGroup.push(this.createAddressGroup(new AddressDelivery()));
  }

  removeDeliveryAddress(index: number): void {
    const addressesDeliveryGroup = this.getAddressDeliveryArray();
    addressesDeliveryGroup.removeAt(index);
  }

  setDefaultDeliveryAddress(selectedIndex: number, status: boolean): void {
    if (status) {
      this.getAddressDeliveryArray().controls.forEach((group: FormGroup, index: number) => {
        if (index !== selectedIndex) {
          group.get('isDefault').setValue(false);
        }
      });
    }
  }

  get debug(): string {
    return JSON.stringify(this.accountCreateForm.getRawValue());
  }
}
