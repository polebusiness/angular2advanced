import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountCreateComponent} from './account-create/account-create.component';
import {AccountComponent} from './account.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from '../core/security/auth-guard.service';
import {AccountLogoutComponent} from './account-logout/account-logout.component';
import {AccountInfosComponent} from './account-infos/account-infos.component';
import {AccountOrdersComponent} from './account-orders/account-orders.component';
import {PasswordChangeComponent} from './password-change/password-change.component';

const routes: Routes = [
  {path: 'account', component: AccountComponent},
  {path: 'login', component: LoginComponent},
  {path: 'account/create', component: AccountCreateComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'infos', component: AccountInfosComponent},
      {path: 'orders', component: AccountOrdersComponent},
      {path: 'password', component: PasswordChangeComponent},
      {path: 'logout', component: AccountLogoutComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {
}
