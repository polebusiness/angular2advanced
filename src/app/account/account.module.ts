import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {SharedModule} from '../shared/shared.module';
import {MenuService, MenuItem} from '../layout/menu/menu.service';

import {AccountRoutingModule} from './account-routing.module';
import {AccountCreateComponent} from './account-create/account-create.component';
import {AccountComponent} from './account.component';
import {PasswordChangeComponent} from './password-change/password-change.component';
import {AccountOrdersComponent} from './account-orders/account-orders.component';
import {UserService} from './shared/user.service';
import {LoginComponent} from './login/login.component';
import {AddressGroupComponent} from './account-create/address-group.component';
import {AccountLogoutComponent} from './account-logout/account-logout.component';
import {AccountInfosComponent} from './account-infos/account-infos.component';

@NgModule({
  imports: [
    AccountRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    AccountCreateComponent,
    AccountComponent,
    PasswordChangeComponent,
    AccountOrdersComponent,
    LoginComponent,
    AddressGroupComponent,
    AccountLogoutComponent,
    AccountInfosComponent
  ],
  providers: [
    UserService
  ]
})
export class AccountModule {
  constructor(menuService: MenuService) {
    menuService.register(new MenuItem('Créer un compte', 'account/create', 20));
  }
}
