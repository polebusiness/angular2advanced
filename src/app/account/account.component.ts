import {Component, OnInit} from '@angular/core';
import {User} from './shared/user.model';
import {Router} from '@angular/router';
import {UserService} from './shared/user.service';
import {SecurityService} from '../core/security/security.service';

@Component({
  selector: 'mp-account',
  templateUrl: 'account.component.html'
})
export class AccountComponent implements OnInit {

  user: User;

  constructor(private securityService: SecurityService,
              private router: Router,
              private userService: UserService) {
  }

  ngOnInit() {

    this.securityService.isConnected.subscribe((connected) => {
      if (connected) {
        this.user = this.securityService.getIdentity<User>();
      } else {
        this.router.navigate(['/login']);
      }
    });
  }
}
