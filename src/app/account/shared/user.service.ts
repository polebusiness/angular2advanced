import {User} from './user.model';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/retry';

@Injectable()
export class UserService {

  private resourceUrl = '/api/user';

  constructor(private httpService: HttpClient) {
  }

  create(user: User): Observable<boolean> {
    return this.httpService.post<boolean>(`${this.resourceUrl}/create`, user);
  }

  authenticate(credentials: { email: string, password: string }): Observable<string> {
    return this.httpService
      .post<{ token: string }>(`${this.resourceUrl}/authenticate`, credentials)
      .retry(2)
      .map((result) => result.token);
  }

  unAuthorised(): Observable<boolean> {
    return this.httpService.get<boolean>(`${this.resourceUrl}/unauthorised`);
  }

  current(): Observable<User> {
    return this.httpService.get<User>(`${this.resourceUrl}/me`);
  }

  changePassword(password: string): Observable<boolean> {
    return this.httpService.post<boolean>(`${this.resourceUrl}/password`, {password});
  }
}
