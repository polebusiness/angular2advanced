import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {UserService} from './user.service';
import {User} from './user.model';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('UserService', () => {

  const user = new User();
  user.gender = 'Mr';
  user.firstName = 'John';
  user.lastName = 'Doe';
  let userService: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    userService = TestBed.get(UserService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should create a new user', fakeAsync(() => {
    // when
    userService.create(user).subscribe();
    const req = httpMock.expectOne('/api/user/create');
    req.flush({});
    tick();

    // then
    expect(req.request.body).toEqual(jasmine.objectContaining(user));
    httpMock.verify();
  }));

  it('Should authenticate user', fakeAsync(() => {
    const authRequest = {email: 'plop@plip', password: 'test'};
    const authResult = {token: '123', user: user};
    let tokenResult: string;

    // when
    userService.authenticate(authRequest).subscribe((token) => {
      tokenResult = token;
    });
    const req = httpMock.expectOne('/api/user/authenticate');
    req.flush(authResult);
    tick();

    // then
    expect(req.request.body).toEqual(jasmine.objectContaining(authRequest));
    expect(tokenResult).toEqual(authResult.token);
    httpMock.verify();
  }));

    it('Should authenticate user if first call fail', fakeAsync(() => {
        const authRequest = {email: 'plop@plip', password: 'test'};
        const authResultSuccess = {token: '123', user: user};
        const authResultError = {};
        let tokenResult: string;

        // when
        userService.authenticate(authRequest).subscribe((token) => {
            tokenResult = token;
        });
        const reqErr = httpMock.expectOne('/api/user/authenticate');
        reqErr.flush(authResultError, {status: 400, statusText: 'simulate error'});
        tick();
        const req = httpMock.expectOne('/api/user/authenticate');
        req.flush(authResultSuccess);
        tick();

        // then
        expect(req.request.body).toEqual(jasmine.objectContaining(authRequest));
        expect(tokenResult).toEqual(authResultSuccess.token);
        httpMock.verify();
    }));

  it('Should get current user', fakeAsync(() => {
    let userResult: User;
    // when
    userService.current().subscribe((result) => {
      userResult = result;
    });
    const req = httpMock.expectOne('/api/user/me');
    req.flush(user);
    tick();

    // then
    expect(userResult).toEqual(jasmine.objectContaining(user));
    httpMock.verify();
  }));

});
