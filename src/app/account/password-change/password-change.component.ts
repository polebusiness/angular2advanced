import {Component, OnInit} from '@angular/core';
import {UserService} from '../shared/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {passwordComplexityValidator} from '../../shared/validator/password-complexity.validator';
import {fieldCompareValidator} from '../../shared/validator/field-compare.directive';

interface PasswordChangeModel {
  current: string;
  change: string;
  confirm: string;
}

@Component({
  selector: 'mp-password-change',
  templateUrl: './password-change.component.html'
})
export class PasswordChangeComponent implements OnInit {

  changePasswordForm: FormGroup;

  passwordModel: PasswordChangeModel = {
    current: '',
    change: '',
    confirm: ''
  };

  displaySuccess = false;

  constructor(private fb: FormBuilder, private userService: UserService) {
  }

  ngOnInit() {
    this.changePasswordForm = this.fb.group({
      current: [null, Validators.required],
      change: [null, [Validators.required, passwordComplexityValidator()]],
      confirm: [null, [Validators.required, fieldCompareValidator(null)]]
    });

    this.changePasswordForm.get('change').valueChanges.subscribe((value) => {
      const confirmControl = this.changePasswordForm.get('confirm');
      confirmControl.setValidators([
        Validators.required, fieldCompareValidator(value)
      ]);
      confirmControl.setValue(confirmControl.value); // force replay validator
    });
  }

  change() {
    this.userService
      .changePassword(this.changePasswordForm.get('change').value)
      .subscribe(() => {
        this.displaySuccess = true;
        this.changePasswordForm.reset();
        setTimeout(() => {
          this.displaySuccess = false;
        }, 3000);
      });
  }
}
