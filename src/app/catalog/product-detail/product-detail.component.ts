import {Component, OnInit, OnDestroy, AfterContentChecked, ViewChild, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../product.model';
import {ProductService} from '../product.service';
import {CartService} from '../../cart/cart.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/bufferTime';
import {Subscription} from 'rxjs/Subscription';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'mp-product-detail',
  templateUrl: './product-detail.component.html'
})
export class ProductDetailComponent implements OnInit, OnDestroy, AfterContentChecked {

  private routeParamSubscription: any;

  quantity = 0;

  product: Product;
  defer: Subscription;
  modalRef: BsModalRef;
  @ViewChild('template') template: TemplateRef<any>;

  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              private cartService: CartService,
              private routerService: Router,
              private modalService: BsModalService
  ) {
  }

  ngOnInit() {
    this.routeParamSubscription = this.route.params.subscribe(params => {
      this.productService.get(params['id'])
        .then((product: Product) => this.product = product);
    });
  }

  ngAfterContentChecked() {
    this.setupDoubleClick();
  }

  setupDoubleClick() {
    const image = document.querySelector('.media');

    if (image && !this.defer) {
      const clickStream = Observable.fromEvent(image, 'click');
      const doubleClickStream = clickStream.bufferTime(350)
        .map(item => item.length)
        .filter(item => item >= 2);

      this.defer = doubleClickStream.subscribe((happen) => {
        this.modalRef = this.modalService.show(this.template, { class: 'modal-lg' });
      });
    }
  }

  ngOnDestroy() {
    this.defer.unsubscribe();
    this.routeParamSubscription.unsubscribe();
  }

  addProduct() {
    if (this.quantity > 0) {
      this.cartService.add({count: this.quantity, product: this.product});
      this.routerService.navigate([{outlets: {popup: ['add-cart-confirm', this.product.id]}}]);
    }
  }
}
