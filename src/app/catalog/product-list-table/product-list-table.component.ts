import {ChangeDetectionStrategy, Component, Input, DoCheck, ChangeDetectorRef} from '@angular/core';
import {Product} from '../product.model';
import {CartService} from '../../cart/cart.service';

@Component({
  selector: 'mp-product-list-table',
  templateUrl: './product-list-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductListTableComponent implements DoCheck {

  @Input() products: Array<Product> = [];

  stockFlag: string;

  constructor(private cartService: CartService, private cd: ChangeDetectorRef) {
  }

  ngDoCheck() {
    // ChangeDetection example. Better implementation with a md5 hash.
    const newFlag = this.products.reduce((flag, product) => flag + product.stock.toString(), '');
    if (newFlag !== this.stockFlag) {
      this.cd.markForCheck();
    }
    this.stockFlag = newFlag;
  }

  addProduct(product: Product) {
    this.cartService.add({product: product, count: 1});
  }
}
