import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CatalogRoutingModule} from './catalog-routing.module';
import {CatalogComponent} from './catalog.component';
import {ProductListModule} from './product-list/product-list.module';
import {MenuItem, MenuService} from '../layout/menu/menu.service';
import {ProductService} from './product.service';
import {ProductDetailComponent} from './product-detail/product-detail.component';
import {SharedModule} from '../shared/shared.module';
import {HttpClientModule} from '@angular/common/http';
import {ProductListTableComponent} from './product-list-table/product-list-table.component';
import {AddCartConfirmComponent} from './add-cart-confirm/add-cart-confirm.component';
import {ModalModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CatalogRoutingModule,
    ProductListModule,
    SharedModule,
    ModalModule.forRoot(),
  ],
  declarations: [
    CatalogComponent,
    ProductDetailComponent,
    ProductListTableComponent,
    AddCartConfirmComponent
  ],
  providers: [
    ProductService
  ]
})
export class CatalogModule {
  constructor(menuService: MenuService) {
    menuService.register(new MenuItem('Catalogue', '/catalog'));
  }
}
