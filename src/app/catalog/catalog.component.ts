import {Component, OnInit} from '@angular/core';
import {Product, ProductCategory} from './product.model';
import {ProductService} from './product.service';

@Component({
  selector: 'mp-catalog',
  templateUrl: './catalog.component.html'
})
export class CatalogComponent implements OnInit {

  categories: Array<{ title: string, subtitle: string, name: ProductCategory }> = [
    {title: 'Apple', subtitle: 'Pour votre pomme !', name: 'apple'},
    {title: 'Windows', subtitle: 'La porte ouverte à toutes les fenêtres !', name: 'windows'},
    {title: 'Premium', subtitle: 'Les meilleurs produits', name: 'premium'},
    {title: 'Bureau', subtitle: 'Quand on a un bureau...', name: 'desktop'},
    {title: 'Portable', subtitle: 'Ou pas !', name: 'laptop'},
    {title: 'Tablettes', subtitle: 'Pour Candy Crush', name: 'laptop'}
  ];

  products: Array<Product>;

  bestSells: Array<Product>;

  promotions: Array<Product>;

  productCategories: { [key: string]: Product[] } = {};

  minPrice = 0;

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.productService
      .getList()
      .subscribe(productList => {
        this.products = productList;
        this.bestSells = this.products.filter(product => product.bestSell);
        this.promotions = this.products.filter(product => product.priceWithoutPromotion);
        this.categories.forEach(category => {
          this.productCategories[category.name] = this.products.filter(product => (
            product.categories && product.categories.find(productCategory => productCategory === category.name)
          ));
        });
      });
  }

  getNoStock() {
    return this.products.filter(product => product.stock === 0);
  }

  simulateDecreaseStock() {
    this.products.forEach(product => product.stock = product.stock > 0 ? product.stock - 1 : 0);
  }
}
