import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CartProduct, CartService} from '../../cart/cart.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'mp-add-cart-confirm',
  templateUrl: './add-cart-confirm.component.html'
})
export class AddCartConfirmComponent implements OnInit, OnDestroy {

  routeSubscription: Subscription;

  cartProduct: CartProduct = null;

  constructor(private route: ActivatedRoute, private cartService: CartService, private router: Router) {
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe((params) => {
      this.cartProduct = this.cartService.getList()
        .find(cartProduct => cartProduct.product.id === parseInt(params['id'], 10));
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  goToCart() {
    this.router.navigate(['', {outlets: {popup: null}}]).then(() => {
      this.router.navigate(['/cart']);
    });
  }
}
