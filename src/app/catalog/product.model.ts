export type ProductCategory = 'windows'|'tablet'|'laptop'|'desktop'|'premium'|'apple';

export interface Product {
  id: number;
  title: string;
  price: number;
  priceWithoutPromotion?: number;
  image: string;
  review: number;
  rating: number;
  categories: ProductCategory[];
  stock?: number;
  bestSell?: boolean;
}
