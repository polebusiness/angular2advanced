import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {SecurityService} from '../../core/security/security.service';

@Directive({
  selector: '[mpIsLogged]'
})
export class IsLoggedDirective implements OnInit {

  @Input() mpIsLogged: boolean;

  constructor(private templateRef: TemplateRef<any>,
              private viewContainerRef: ViewContainerRef,
              private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.isConnected.subscribe(
      status => {
        if (status === this.mpIsLogged) {
          this.viewContainerRef.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainerRef.clear();
        }
      }
    );
  }
}
