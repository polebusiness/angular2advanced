import {IsLoggedDirective} from './is-logged.directive';
import {Component} from '@angular/core';
import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SecurityService} from '../../core/security/security.service';
import {Subject} from 'rxjs/Subject';
import {By} from '@angular/platform-browser';

@Component({template: '<div *mpIsLogged="status"><p>content</p></div>'})
class FakeComponent {
  status: boolean;
}

describe('IsLoggedDirective', () => {

  let component: ComponentFixture<FakeComponent>;
  let securityService: SecurityService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FakeComponent, IsLoggedDirective],
      providers: [{
        provide: SecurityService, useValue: {
          isConnected: new Subject<boolean>()
        }
      }]
    });
    component = TestBed.createComponent(FakeComponent);
    securityService = TestBed.get(SecurityService);
    component.detectChanges();
  });

  const runCase = (status, isConnected, expected) => {
    component.componentInstance.status = status;
    component.detectChanges();

    securityService.isConnected.next(isConnected);

    expect(component.debugElement.queryAll(By.css('p')).length).toBe(expected);
  };

  it('should show element when logged and status true', () => runCase(true, true, 1));
  it('should show element when not logged and status false', () => runCase(false, false, 1));
  it('should hide element when not logged and status true', () => runCase(true, false, 0));
  it('should hide element when logged and status false', () => runCase(false, true, 0));
});

