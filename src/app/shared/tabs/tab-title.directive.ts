import {Directive, TemplateRef} from '@angular/core';

@Directive({
  selector: '[mpTabTitle]'
})
export class TabTitleDirective {
  constructor(public templateRef: TemplateRef<any>) {
  }
}
