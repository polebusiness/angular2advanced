import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabsComponent} from './tabs.component';
import {TabComponent} from './tab.component';
import {TabTitleDirective} from './tab-title.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TabsComponent, TabComponent, TabTitleDirective],
  exports: [TabsComponent, TabComponent, TabTitleDirective]
})
export class TabsModule {
}
