import {Component, ContentChild} from '@angular/core';
import {TabTitleDirective} from './tab-title.directive';

@Component({
  selector: 'mp-tab',
  template: `
    <div class="tab-content">
      <div class="tab-pane" [class.active]="selected">
        <ng-content></ng-content>
      </div>
    </div>
  `
})
export class TabComponent {

  @ContentChild(TabTitleDirective) tabTitleElement: TabTitleDirective;

  selected = false;

  getTitleTemplateRef() {
    if (this.tabTitleElement) {
      return this.tabTitleElement.templateRef;
    } else {
      throw new Error('No tab title defined on element');
    }
  }
}
