import {
  AfterContentInit, AfterViewInit, Component, ContentChildren, QueryList, ViewChildren, ViewContainerRef
} from '@angular/core';
import {TabComponent} from './tab.component';

@Component({
  selector: 'mp-tabs',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item" *ngFor="let tab of tabs">
        <a class="nav-link" [class.active]="tab.selected" (click)="selectTab(tab)">
          <ng-container #vc></ng-container>
        </a>
      </li>
    </ul>
    <ng-content></ng-content>
  `
})
export class TabsComponent implements AfterContentInit, AfterViewInit {

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  @ViewChildren('vc', {read: ViewContainerRef}) tabViewRef: Array<ViewContainerRef>;

  ngAfterContentInit() {
    if (this.tabs.length > 0) {
      this.selectTab(this.tabs.first);
    }
  }

  ngAfterViewInit(): void {
    this.tabViewRef.forEach((vcr, index) => {
      vcr.createEmbeddedView(this.tabs.toArray()[index].getTitleTemplateRef());
      vcr.get(0).detectChanges();
    });
  }

  selectTab(selectedTab) {
    this.tabs.forEach((tab) => tab.selected = false);
    selectedTab.selected = true;
  }
}
