import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TabsComponent} from './tabs.component';
import {TabComponent} from './tab.component';
import {Component} from '@angular/core';
import {By} from '@angular/platform-browser';
import {TabTitleDirective} from './tab-title.directive';

@Component({
  template: `
    <mp-tabs>
      <mp-tab><span *mpTabTitle>tab 1</span><p>content 1</p></mp-tab>
      <mp-tab><span *mpTabTitle>tab 2</span><p>content 2</p></mp-tab>
    </mp-tabs>`
})
class FakeComponent {
}

describe('TabsComponent', () => {

  let component: TabsComponent;
  let fixture: ComponentFixture<FakeComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TabsComponent, TabComponent, FakeComponent, TabTitleDirective]
    });
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.debugElement.query(By.css('mp-tabs')).componentInstance;
    fixture.detectChanges();
  });

  it('Should register tabs', () => {
    expect(component.tabs.length).toBe(2);
  });

  it('Should display tabs', () => {
    expect(fixture.debugElement.queryAll(By.css('ul.nav-tabs .nav-item')).length).toBe(2);
  });

  it('Should display tabs title attributes in nav items', () => {
    const navItems = fixture.debugElement.queryAll(By.css('ul.nav-tabs .nav-item .nav-link'));
    expect(navItems[0].nativeElement.textContent.trim()).toBe('tab 1');
    expect(navItems[1].nativeElement.textContent.trim()).toBe('tab 2');
  });

  it('Should add active css class on nav item and container', () => {
    const navItem = fixture.debugElement.query(By.css('ul.nav-tabs .nav-link'));
    const tabContainer = fixture.debugElement.query(By.css('.tab-content .tab-pane'));

    navItem.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(navItem.nativeElement.classList.contains('active')).toBeTruthy();
    expect(tabContainer.nativeElement.classList.contains('active')).toBeTruthy();
  });

  it('Should have only one selected tab', () => {
    const navItems = fixture.debugElement.queryAll(By.css('ul.nav-tabs .nav-link'));

    navItems[0].triggerEventHandler('click', null);
    fixture.detectChanges();

    navItems[1].triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(fixture.debugElement.queryAll(By.css('ul.nav-tabs .nav-link.active')).length).toBe(1);
    expect(fixture.debugElement.queryAll(By.css('.tab-content .tab-pane.active')).length).toBe(1);
  });

  it('Should select first tab by default', () => {
    const navItem = fixture.debugElement.query(By.css('ul.nav-tabs .nav-link'));
    const tabContainer = fixture.debugElement.query(By.css('.tab-content .tab-pane'));

    fixture.detectChanges();

    expect(navItem.nativeElement.classList.contains('active')).toBeTruthy();
    expect(tabContainer.nativeElement.classList.contains('active')).toBeTruthy();
  });
});
