import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'mp-rating',
  template: `
    <i *ngFor="let rate of rateRange()" class="glyphicon glyphicon-star"></i>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RatingComponent {
  @Input() rating: number;

  rateRange(): Array<number> {
    return new Array(this.rating);
  }
}
