import {NgModule} from '@angular/core';
import {PasswordComplexityDirective} from './password-complexity.directive';
import {FieldCompareDirective} from './field-compare.directive';
export {passwordComplexityValidator} from './password-complexity.validator';

@NgModule({
  declarations: [PasswordComplexityDirective, FieldCompareDirective],
  exports: [PasswordComplexityDirective, FieldCompareDirective]
})
export class ValidatorModule {
}
