import {FormControl, ValidatorFn} from '@angular/forms';
import {passwordComplexityValidator} from './password-complexity.validator';

describe('Shared Module: passwordComplexityValidator', () => {

  let validator: ValidatorFn;
  let control: FormControl;
  beforeEach(() => {
    validator = passwordComplexityValidator();
    control = new FormControl();
  });

  it('Should pass all default rules', () => {
    // Given
    control.setValue('Test123!');

    // When
    const result = validator(control);

    // then
    expect(result).toBe(null);
  });

  it('Should fail number rule', () => {
    // Given
    control.setValue('Testtest!');

    // When
    const result = validator(control);

    // then
    expect(result).not.toBe(null);
    expect(result.passwordComplexity.rulesResults.haveNumber).toBe(false);
  });

  it('Should fail length rule', () => {
    // Given
    control.setValue('Te123!');

    // When
    const result = validator(control);

    // then
    expect(result).not.toBe(null);
    expect(result.passwordComplexity.rulesResults.minLength).toBe(false);
  });

  it('Should fail lower and upper case rule', () => {
    // Given
    control.setValue('test123!');

    // When
    const result = validator(control);

    // then
    expect(result).not.toBe(null);
    expect(result.passwordComplexity.rulesResults.haveLowerAndUpperChar).toBe(false);
  });

  it('Should fail special char rule', () => {
    // Given
    control.setValue('test1234');

    // When
    const result = validator(control);

    // then
    expect(result).not.toBe(null);
    expect(result.passwordComplexity.rulesResults.haveSpecialChar).toBe(false);
  });

  it('Should pass with rules disabled', () => {
    // Given
    validator = passwordComplexityValidator([
      {key: 'haveSpecialChar', enabled: false},
      {key: 'haveLowerAndUpperChar', enabled: false},
      {key: 'minLength', enabled: false},
      {key: 'haveNumber', enabled: false},
    ]);
    control.setValue('test1234');

    // When
    const result = validator(control);

    // then
    expect(result).toBe(null);
  });

  it('Should check additional rule', () => {
    // Given
    validator = passwordComplexityValidator([
      {key: 'doesNotContainTest', validate: (value: any) => !String(value).match(/test/ig)},
    ]);
    control.setValue('Test123!');

    // When
    const result = validator(control);

    // then
    expect(result).not.toBe(null);
    expect(result.passwordComplexity.rulesResults.doesNotContainTest).toBe(false);
  });

  it('Should override default rule param', () => {
    // Given
    validator = passwordComplexityValidator([
      {key: 'minLength', param: 10},
    ]);
    control.setValue('Test123!');

    // When
    const result = validator(control);

    // then
    expect(result).not.toBe(null);
    expect(result.passwordComplexity.rulesResults.minLength).toBe(false);
  });
});
