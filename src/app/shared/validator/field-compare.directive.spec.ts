import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule, NgModel} from '@angular/forms';
import {Component, ViewChild} from '@angular/core';
import {FieldCompareDirective} from './field-compare.directive';

@Component({
  template: `
    <input #modelRef="ngModel" [(ngModel)]="fieldRef"/>
    <input #model="ngModel" [mpFieldCompare]="fieldRef" [(ngModel)]="field"/>
  `
})
class StubComponent {
  @ViewChild('modelRef') modelRef: NgModel;
  @ViewChild('model') model: NgModel;
  fieldRef: string;
  field: string;
}

describe('Shared Module: FieldCompareDirective', () => {

  let fixture: ComponentFixture<StubComponent>;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [FieldCompareDirective, StubComponent]
    }).createComponent(StubComponent);
  });

  it('Should add error when field are different', () => {
    // Given
    fixture.debugElement.componentInstance.fieldRef = 'test';
    fixture.detectChanges();

    // When
    fixture.debugElement.componentInstance.model.control.setValue('test1234');
    fixture.detectChanges();

    // Then
    expect(fixture.debugElement.componentInstance.model.hasError('fieldCompare')).toBe(true);
  });

  it('Should not add error when field are equal', () => {
    // Given
    fixture.debugElement.componentInstance.fieldRef = 'test';
    fixture.detectChanges();

    // When
    fixture.debugElement.componentInstance.model.control.setValue('test');
    fixture.detectChanges();

    // Then
    expect(fixture.debugElement.componentInstance.model.hasError('fieldCompare')).toBe(false);
  });

  it('Should add error when ref field change to a different value', () => {
    // Given
    fixture.debugElement.componentInstance.fieldRef = 'test';
    fixture.debugElement.componentInstance.field = 'test';
    fixture.detectChanges();

    // When
    fixture.debugElement.componentInstance.modelRef.control.setValue('test1234');
    fixture.detectChanges();

    // Then
    expect(fixture.debugElement.componentInstance.model.hasError('fieldCompare')).toBe(true);
  });
});
