import {ComponentFixture, TestBed} from '@angular/core/testing';
import {PasswordComplexityDirective} from './password-complexity.directive';
import {FormsModule, NgModel} from '@angular/forms';
import {Component, ViewChild} from '@angular/core';
import {ComplexityRule} from './password-complexity.validator';

@Component({
  template: '<input #model="ngModel" [mpPasswordComplexity]="rulesConfig" [(ngModel)]="pass" />'
})
class StubComponent {
  @ViewChild('model') model: NgModel;
  pass: string;
  rulesConfig: Array<ComplexityRule>;
}

describe('Shared Module: PasswordComplexityDirective', () => {

  let fixture: ComponentFixture<StubComponent>;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [PasswordComplexityDirective, StubComponent]
    }).createComponent(StubComponent);
  });

  it('Should apply match validator complexity rules against model value', () => {
    // Given
    fixture.debugElement.componentInstance.model.control.setValue('test1234');

    // When
    fixture.detectChanges();
    const error = fixture.debugElement.componentInstance.model.getError('passwordComplexity');

    // Then
    expect(fixture.debugElement.componentInstance.model.hasError('passwordComplexity')).toBe(true);
    expect(error.rulesResults.minLength).toBe(true);
    expect(error.rulesResults.haveNumber).toBe(true);
    expect(error.rulesResults.haveSpecialChar).toBe(false);
    expect(error.rulesResults.haveLowerAndUpperChar).toBe(false);
  });

  it('Should override default rules', () => {
    // Given
    fixture.debugElement.componentInstance.rulesConfig = [
      {key: 'haveSpecialChar', enabled: false},
      {key: 'haveLowerAndUpperChar', enabled: false},
    ];
    fixture.detectChanges();

    // When
    fixture.debugElement.componentInstance.model.control.setValue('test1234');
    fixture.detectChanges();

    // Then
    expect(fixture.debugElement.componentInstance.model.hasError('passwordComplexity')).toBe(false);
  });
});
