import {Directive, Input, OnChanges, SimpleChanges} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';
import {ComplexityRule, passwordComplexityValidator} from './password-complexity.validator';

@Directive({
  selector: '[mpPasswordComplexity]',
  providers: [{provide: NG_VALIDATORS, useExisting: PasswordComplexityDirective, multi: true}]
})
export class PasswordComplexityDirective implements Validator, OnChanges {
  @Input('mpPasswordComplexity') mpPasswordComplexity: Array<ComplexityRule> = [];
  private validateFn: ValidatorFn = passwordComplexityValidator();

  validate(control: AbstractControl): ValidationErrors {
    return this.validateFn(control);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const change = changes['mpPasswordComplexity'];
    if (change && Array.isArray(change.currentValue)) {
      this.validateFn = passwordComplexityValidator(change.currentValue);
    }
  }
}
