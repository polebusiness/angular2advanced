import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export interface ComplexityRule {
  key: string;
  param?: any;
  enabled?: boolean;
  validate?: (value: any, param?: any) => boolean;
}

interface RuleResult {
  key: string;
  status: boolean;
}

const defaultRules: Array<ComplexityRule> = [
  {key: 'minLength', param: 8, enabled: true, validate: (value, param) => String(value).length >= parseInt(param, 10)},
  {key: 'haveNumber', enabled: true, validate: (value) => !!String(value).match(/\d+/g)},
  {key: 'haveSpecialChar', enabled: true, validate: (value) => !!String(value).match(/[^\w]+/g)},
  {
    key: 'haveLowerAndUpperChar',
    enabled: true,
    validate: (value, param) => !!String(value).match(/([a-z]+.*[A-Z]+.*)|([A-Z]+.*[a-z]+.*)/g)
  },
];

export function passwordComplexityValidator(rulesConfig: Array<ComplexityRule> = []): ValidatorFn {
  const rules = [...defaultRules];
  rulesConfig.forEach((rule): void => {
    const indexDefaultRule = rules.findIndex((item) => item.key === rule.key);
    if (indexDefaultRule !== -1) {
      rules[indexDefaultRule] = {...rules[indexDefaultRule], ...rule};
    } else {
      rules.push(rule);
    }
  });

  return (control: AbstractControl): ValidationErrors => {
    const password = control.value || '';
    const rulesResults: Array<RuleResult> = [];
    let result = true;

    rules.forEach((rule): void => {
      const param = typeof rule.param !== 'undefined' ? rule.param : null;
      const enabled = typeof rule.enabled !== 'undefined' ? rule.enabled : true;
      if (enabled) {
        if (!rule.validate) {
          throw new Error(`Enabled password complexity ${rule.key} rule without validate method`);
        }
        rulesResults[rule.key] = rule.validate(password, param);
        result = rulesResults[rule.key] && result;
      }
    });

    return !result ? {'passwordComplexity': {rulesResults}} : null;
  };
}
