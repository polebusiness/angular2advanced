import {Directive, Input, OnChanges, SimpleChanges} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from '@angular/forms';

export function fieldCompareValidator(comparedValue: any): ValidatorFn {
  return (control: AbstractControl): ValidationErrors => {
    const basedValue = control.value || '';
    const test = basedValue === comparedValue;
    return !test ? {'fieldCompare': true} : null;
  };
}

@Directive({
  selector: '[mpFieldCompare]',
  providers: [{provide: NG_VALIDATORS, useExisting: FieldCompareDirective, multi: true}]
})
export class FieldCompareDirective implements OnChanges {
  @Input('mpFieldCompare') mpFieldCompare: any;
  private validateFn: ValidatorFn = fieldCompareValidator(null);
  private formControl: AbstractControl;

  validate(control: AbstractControl): ValidationErrors {
    // save related control in case we need to re validate on ref model change
    this.formControl = control;
    return this.validateFn(control);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const change = changes['mpFieldCompare'];
    if (change) {
      this.validateFn = fieldCompareValidator(change.currentValue);
      // Manually trigger validators since reference has changed
      if (this.formControl) {
        this.formControl.setValue(this.formControl.value);
      }
    }
  }
}
