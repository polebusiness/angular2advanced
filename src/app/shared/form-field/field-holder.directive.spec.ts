import {FieldHolderDirective} from './field-holder.directive';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {Component} from '@angular/core';
import {By} from '@angular/platform-browser';
import {FormsModule, NgModel} from '@angular/forms';

@Component({
  template: '<input mpFieldHolder name="test" ngModel="test" />'
})
class FakeComponent {
}

describe('FieldHolderDirective', () => {

  let fixture: ComponentFixture<FakeComponent>;
  let directiveInstance: FieldHolderDirective;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [FieldHolderDirective, FakeComponent]
    });
    fixture = TestBed.createComponent(FakeComponent);
    fixture.detectChanges();
    const directiveEl = fixture.debugElement.query(By.directive(FieldHolderDirective));
    directiveInstance = directiveEl.injector.get(FieldHolderDirective);
  });

  it('should add default type text on input', () => {
    expect(
      fixture.debugElement.query(By.css('input')).nativeElement.getAttribute('type')
    ).toBe('text');
    expect(directiveInstance.getType()).toBe('text');
  });

  it('should return ngModel instance', () => {
    expect(directiveInstance.getNgModel() instanceof NgModel).toBeTruthy();
  });

  it('should add form-control class on element', () => {
    expect(
      fixture.debugElement.query(By.css('input')).nativeElement.classList.contains('form-control')
    ).toBe(true);
  });

  it('should return generated id on input', () => {
    expect(
      directiveInstance.getId()
    ).toBeTruthy(
      fixture.debugElement.query(By.css('input')).nativeElement.getAttribute('id')
    );
  });
});
