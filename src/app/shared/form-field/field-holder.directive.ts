import {ContentChild, Directive, ElementRef} from '@angular/core';
import {NgModel} from '@angular/forms';

@Directive({
  selector: '[mpFieldHolder]'
})
export class FieldHolderDirective {

  @ContentChild(NgModel) model: NgModel;

  constructor(private elementRef: ElementRef) {

    const name = this.elementRef.nativeElement.getAttribute('name');

    if (!this.getType()) {
      this.elementRef.nativeElement.setAttribute('type', 'text');
    }

    this.elementRef.nativeElement.classList.add('form-control');
    this.elementRef.nativeElement.setAttribute('id', `${name}-${Date.now()}`);
  }

  getType(): string {
    return this.elementRef.nativeElement.getAttribute('type');
  }

  getId(): string {
    return this.elementRef.nativeElement.getAttribute('id');
  }

  getNgModel(): NgModel {
    return this.model;
  }
}
