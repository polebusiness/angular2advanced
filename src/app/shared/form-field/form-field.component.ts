import {AfterContentInit, Component, ContentChild, Input} from '@angular/core';
import {FieldHolderDirective} from './field-holder.directive';

interface ErrorMessage {
  type: string;
  message: string;
}

const errorMessages: Array<ErrorMessage> = [
  {type: 'required', message: 'Ce champ est requis'},
  {type: 'email', message: 'Ceci n\'est pas une email valide'}
];

@Component({
  selector: 'mp-form-field',
  templateUrl: './form-field.component.html'
})
export class FormFieldComponent implements AfterContentInit {

  @Input() label: string;

  @ContentChild(FieldHolderDirective) field: FieldHolderDirective;

  fieldId: string;

  ngAfterContentInit() {
    this.fieldId = this.field.getId();
  }

  get errors(): Array<ErrorMessage> {
    const errorList = this.field.getNgModel().errors || {};
    return Object.keys(errorList)
      .map(key => ({key, status: errorList[key]}))
      .filter(error => error.status)
      .map(error => errorMessages.find(errorMessage => error.key === errorMessage.type));
  }
}
