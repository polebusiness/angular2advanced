import {Component} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormFieldComponent} from './form-field.component';
import {FieldHolderDirective} from './field-holder.directive';
import {By} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';

@Component({
  template: '<mp-form-field label="test"><input mpFieldHolder /></mp-form-field>'
})
class FakeComponent {
}

describe('FormFieldComponent', () => {

  let fixture: ComponentFixture<FakeComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule],
      declarations: [FieldHolderDirective, FormFieldComponent, FakeComponent]
    });
    fixture = TestBed.createComponent(FakeComponent);
  });

  it('should do content projection', () => {
    expect(fixture.debugElement.queryAll(By.css('.form-group input')).length).toBe(1);
  });

  it('should display model errors', () => {

    const inputElement = fixture.debugElement.query(By.directive(FieldHolderDirective));
    const directiveInstance = inputElement.injector.get(FieldHolderDirective);
    const spy = spyOn(directiveInstance, 'getNgModel').and.returnValue({
      touched: true,
      invalid: true,
      errors: {required: true}
    });

    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('.alert-danger li')).nativeElement.textContent)
      .toBe('Ce champ est requis');
  });

  it('should set the label for attribute with the input id', () => {

    const inputElement = fixture.debugElement.query(By.directive(FieldHolderDirective));
    const directiveInstance = inputElement.injector.get(FieldHolderDirective);
    const spyNgModel = spyOn(directiveInstance, 'getNgModel').and.returnValue({touched: false});

    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.css('label')).nativeElement.getAttribute('for'))
      .toBe(inputElement.nativeElement.getAttribute('id'));
  });
});
