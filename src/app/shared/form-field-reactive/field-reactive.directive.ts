import {AfterContentInit, ContentChild, Directive, ElementRef} from '@angular/core';
import {FormControlName} from '@angular/forms';

@Directive({
  selector: '[mpFieldReactive]'
})
export class FieldReactiveDirective implements AfterContentInit {

  @ContentChild(FormControlName) formControl: FormControlName;

  constructor(private elementRef: ElementRef) {
  }

  ngAfterContentInit(): void {
    this.elementRef.nativeElement.addEventListener('focus', () => this.formControl.control.markAsUntouched());
  }

  getAttributeName(): string {
    const name = this.elementRef.nativeElement.getAttribute('formcontrolname');
    if (!name) {
      throw new Error('No formControlName defined on input element.');
    }
    return name;
  }

  getType(): string {
    return this.elementRef.nativeElement.getAttribute('type');
  }

  setType(type: string): void {
    return this.elementRef.nativeElement.setAttribute('type', type);
  }

  setId(id: string): void {
    this.elementRef.nativeElement.setAttribute('id', id);
  }

  addClass(cssClass: string) {
    this.elementRef.nativeElement.classList.add(cssClass);
  }
}
