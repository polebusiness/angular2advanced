import {AfterContentChecked, Component, ContentChild, Input} from '@angular/core';
import {FieldReactiveDirective} from './field-reactive.directive';

interface FormError {
  key: string;
  label: string;
}

const errorMessages: Array<FormError> = [
  {key: 'required', label: 'Ce champ est requis'},
  {key: 'email', label: 'Cette adresse email n\'est pas valide'},
  {key: 'default', label: 'Ce champs n\'est pas valide'},
  {key: 'passwordComplexity', label: 'Ce champs ne respecte pas les critères de complexité'},
  {key: 'fieldCompare', label: 'La valeur n\'est pas identique'},
];

@Component({
  selector: 'mp-form-field-reactive',
  template: `
    <div class="form-group">
      <label *ngIf="label" for="{{fieldId}}">{{label}}</label>
      <ng-content></ng-content>
      <ng-container *ngIf="input.formControl.invalid && input.formControl.touched">
        <ng-container *ngFor="let error of getErrors()">
          <p class="alert alert-danger {{error.key}}">{{error.label}}</p>
        </ng-container>
      </ng-container>
    </div>
  `
})
export class FormFieldReactiveComponent implements AfterContentChecked {
  @Input() label: string;
  @ContentChild(FieldReactiveDirective) input: FieldReactiveDirective;

  fieldId: string;

  ngAfterContentChecked(): void {

    if (!this.input) {
      throw new Error('No FormFieldReactiveComponent directive found on form-input content.');
    }

    if (!this.input.getType()) {
      this.input.setType('text');
    }

    if (!this.input.getType().match(/checkbox|radio/)) {
      this.fieldId = `${this.input.getAttributeName()}-${Date.now()}`;
      this.input.setId(this.fieldId);
      this.input.addClass('form-control');
    }
  }

  getErrors(): Array<FormError> {
    const controlErrors = this.input.formControl.errors;
    let errors: Array<FormError> = [];

    if (controlErrors) {
      errors = errorMessages.filter((error: FormError) => {
        return controlErrors.hasOwnProperty(error.key) && controlErrors[error.key];
      });
    }
    return errors;
  }
}
