import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {HighlighterDirective} from './highlighter/highlighter.directive';
import {GreaterThanFilterPipe} from './greater-than-filter/greater-than-filter.pipe';
import {CounterComponent} from './counter.component';
import {FormFieldComponent} from './form-field/form-field.component';
import {FieldHolderDirective} from './form-field/field-holder.directive';
import {TabsModule} from './tabs/tabs.module';
import {RatingComponent} from './rating/rating.component';
import {IsLoggedDirective} from './security/is-logged.directive';
import {FormFieldReactiveComponent} from './form-field-reactive/form-field-reactive.component';
import {FieldReactiveDirective} from './form-field-reactive/field-reactive.directive';
import {ValidatorModule} from './validator/validator.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    TabsModule,
    ValidatorModule
  ],
  declarations: [
    HighlighterDirective,
    GreaterThanFilterPipe,
    CounterComponent,
    FormFieldComponent,
    FieldHolderDirective,
    RatingComponent,
    IsLoggedDirective,
    FormFieldReactiveComponent,
    FieldReactiveDirective
  ],
  exports: [
    HighlighterDirective,
    GreaterThanFilterPipe,
    CounterComponent,
    FormFieldComponent,
    FieldHolderDirective,
    TabsModule,
    RatingComponent,
    IsLoggedDirective,
    FormFieldReactiveComponent,
    FieldReactiveDirective,
    ValidatorModule
  ]
})
export class SharedModule {
}
