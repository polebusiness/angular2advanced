import {TestBed} from '@angular/core/testing';
import {UserService} from '../../account/shared/user.service';
import {SecurityService} from '../security/security.service';
import {authInitializer} from './auth-initializer';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Auth initializer', () => {
  let securityService: SecurityService;
  let userService: UserService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService, SecurityService]
    });
    securityService = TestBed.get(SecurityService);
    userService = TestBed.get(UserService);
  });

  it('should return a promise factory', () => {
    const initializer = authInitializer(securityService, userService)();
    expect(initializer['then']).toBeDefined();
  });

  it('should call reconnect method', () => {
    spyOn(securityService, 'reconnect').and.callFake(() => {
      return new Promise(() => {});
    });

    const initializer = authInitializer(securityService, userService)();

    expect(securityService.reconnect).toHaveBeenCalled();
  });
});
