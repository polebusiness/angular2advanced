import {SecurityService} from '../security/security.service';
import {UserService} from '../../account/shared/user.service';

export function authInitializer(securityService: SecurityService,
                                userService: UserService) {
  return () => securityService.reconnect(userService.current());
}
