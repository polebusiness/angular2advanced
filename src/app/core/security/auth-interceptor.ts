import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {SecurityService} from './security.service';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private securityService: SecurityService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.securityService.getAuthToken();
    let newReq = req;

        return Observable.fromPromise(this.securityService.getAuthTokenPromise())
            .switchMap((token) => {
                const authReq = req.clone({
                    headers: req.headers.set('authorization', `Bearer ${token}`)
                });
                return next.handle(authReq);
            })
            .catch(error => next.handle(req));
    }
}
