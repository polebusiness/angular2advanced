import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {UnauthorizedInterceptor} from './unauthorized-interceptor';
import {RouterTestingModule} from '@angular/router/testing';
import {SecurityService} from './security.service';
import {Location} from '@angular/common';
import {Component} from '@angular/core';

@Component({template: ''})
class FakeComponent {
}

describe('UnauthorizedInterceptor', () => {

  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let location: Location;
  let securityService: SecurityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([{path: 'login', component: FakeComponent}])
      ],
      declarations: [FakeComponent],
      providers: [
        {provide: HTTP_INTERCEPTORS, useClass: UnauthorizedInterceptor, multi: true},
        SecurityService
      ]
    });
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
    securityService = TestBed.get(SecurityService);
    location = TestBed.get(Location);
  });

  it('Should call logout method and redirect to logout on 401', fakeAsync(() => {

    spyOn(securityService, 'logout').and.callFake(() => true);
    httpClient.get('/test').subscribe(() => false, () => false);
    const req = httpMock.expectOne('/test');

    req.flush({}, {status: 401, statusText: 'unauthorized'});
    tick();

    expect(securityService.logout).toHaveBeenCalled();
    expect(location.path()).toBe('/login');
  }));

  it('Should add authorization header when logged in', fakeAsync(() => {
    spyOn(securityService, 'logout').and.callFake(() => true);
    httpClient.get('/test').subscribe(() => false, () => false);
    const req = httpMock.expectOne('/test');

    req.flush({});
    tick();

    expect(securityService.logout).not.toHaveBeenCalled();
    expect(location.path()).not.toBe('/login');
  }));
});
