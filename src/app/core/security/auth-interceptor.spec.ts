import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SecurityService} from './security.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {AuthInterceptor} from './auth-interceptor';

class SecurityMockService {
  token: string = null;

  getAuthToken() {
    return this.token;
  }

  getAuthTokenPromise() {
      return new Promise((resolve, reject) => {
          this.token ? resolve(this.token) : reject();
      });
  }
}

describe('AuthInterceptor', () => {

  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let securityService: SecurityMockService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: SecurityService, useClass: SecurityMockService},
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
      ]
    });
    securityService = TestBed.get(SecurityService);
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
  });

  it('Should not add authorization header when not logged in', fakeAsync(() => {
    httpClient.get('/test').subscribe();
    const req = httpMock.expectOne('/test');

    req.flush({});

    expect(req.request.headers.get('authorization')).toBe(null);
  }));

  it('Should add authorization header when logged in', fakeAsync(() => {
    securityService.token = '123';
    httpClient.get('/test').subscribe();
    const req = httpMock.expectOne('/test');

    req.flush({});

    expect(req.request.headers.get('authorization')).toBe('Bearer 123');
  }));
});
