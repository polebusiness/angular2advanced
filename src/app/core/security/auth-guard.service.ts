import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {SecurityService} from './security.service';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private securityService: SecurityService,
              private routerService: Router) {
  }

  canActivate() {
    return this.securityService.isConnected.pipe<boolean>(
      map((isLogged) => {
        if (!isLogged) {
          this.routerService.navigate(['/login']);
        }
        return isLogged;
      })
    );
  }
}
