import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

export interface Authenticatable {
  roles: Array<string>;
}

@Injectable()
export class SecurityService {

  private localStorageKey = 'authorizationToken';

  private identity: Authenticatable = null;

  private authToken: string = null;

  isConnected: BehaviorSubject<boolean> = new BehaviorSubject(false);

  login(token: string, identityProvider: Observable<Authenticatable>): Observable<boolean> {
    const loginObserver = new Subject<boolean>();
    this.authToken = token;

    identityProvider
      .subscribe(identity => {
        this.identity = identity;
        localStorage.setItem(this.localStorageKey, token);
        this.isConnected.next(true);
        loginObserver.next(true);
      }, () => {
        this.authToken = null; // invalid token
        loginObserver.error('unexpected login error');
      }, () => {
        loginObserver.complete();
      });

    return loginObserver;
  }

  logout(): void {
    this.isConnected.next(false);
    localStorage.removeItem(this.localStorageKey);
    this.authToken = null;
    this.identity = null;
  }

  getAuthToken(): string {
    return this.authToken;
  }

  getAuthTokenPromise(): Promise<string|Error> {
      return new Promise((resolve, reject) => {
          this.authToken ? resolve(this.authToken) : reject();
      });
  }

  getIdentity<T extends Authenticatable>(): T {
    return this.identity as T;
  }

  updateIdentity<T extends Authenticatable>(identity: T): void {
    this.identity = identity;
  }

  reconnect(identityProvider: Observable<Authenticatable>): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const token = localStorage.getItem(this.localStorageKey);
      if (token) {
        this.login(token, identityProvider).subscribe(resolve, resolve);
      } else {
        resolve(true);
      }
    });
  }
}
