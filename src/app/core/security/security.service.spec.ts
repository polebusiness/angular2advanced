import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Authenticatable, SecurityService} from './security.service';
import {Subject} from 'rxjs/Subject';

class Identity implements Authenticatable {
  roles = [];
}

describe('SecurityService', () => {

  const user = new Identity();
  let IdentityObserver: Subject<Identity>;
  let securityService: SecurityService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [SecurityService]
    });
    securityService = TestBed.get(SecurityService);
    IdentityObserver =  new Subject<Identity>();
  });

  it('Should have no user logged in by default', fakeAsync(() => {
    let isConnected: boolean;

    securityService.isConnected.subscribe((status) => isConnected = status);
    tick();

    expect(isConnected).toBe(false);
  }));

  it('Should have connected user after login', fakeAsync(() => {
    let isConnected: boolean;
    securityService.isConnected.subscribe((status) => isConnected = status);

    securityService.login('123', IdentityObserver);
    IdentityObserver.next(user);
    tick();

    expect(isConnected).toBe(true);
    expect(securityService.getIdentity()).toEqual(jasmine.objectContaining(user));
    expect(securityService.getAuthToken()).toEqual('123');
  }));

  it('Should not have connected user after logout', fakeAsync(() => {
    let isConnected: boolean;
    securityService.isConnected.subscribe((status) => isConnected = status);
    securityService.login('123', IdentityObserver);
    IdentityObserver.next(user);
    tick();

    securityService.logout();
    tick();

    expect(isConnected).toBe(false);
    expect(securityService.getIdentity()).toEqual(null);
    expect(securityService.getAuthToken()).toEqual(null);
  }));

});
