import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import {ConfigService} from './config.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('ConfigService', () => {

  let httpMock: HttpTestingController;
  let configService: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ConfigService]
    });
    httpMock = TestBed.get(HttpTestingController);
    configService = TestBed.get(ConfigService);
  });

  it('should init with web service result', fakeAsync(() => {
    configService.init();
    const req = httpMock.expectOne('/api/config');

    req.flush({
      'test': 'plop'
    });
    tick();

    expect(configService.get('test')).toBe('plop');
  }));

  it('should return null for unknown key', fakeAsync(() => {
    configService.init();
    const req = httpMock.expectOne('/api/config');

    req.flush({
      'test': 'plop'
    });
    tick();

    expect(configService.get('toto')).toBe(null);
  }));
});
