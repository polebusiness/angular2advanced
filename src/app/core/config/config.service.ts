import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ConfigService {

  private baseUrl = '/api/config';

  private store: Object = {};

  constructor(private httpClient: HttpClient) {
  }

  init(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.httpClient.get<Object>(this.baseUrl)
        .subscribe(
          configs => {
            this.store = Object.assign(this.store, configs);
            resolve(true);
          },
          resolve
        );
    });
  }

  get(key: string): any {
    return typeof this.store[key] !== 'undefined' ? this.store[key] : null;
  }
}
