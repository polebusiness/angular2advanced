import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ConfigService} from './config.service';
import {configInitializer} from './config-initializer';

describe('Config initializer', () => {
  let configService: ConfigService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ConfigService]
    });
    configService = TestBed.get(ConfigService);
  });

  it('should return a promise factory', () => {
    const initializer = configInitializer(configService)();
    expect(initializer['then']).toBeDefined();
  });

  it('should call init method', () => {
    spyOn(configService, 'init').and.callFake(() => {
      return new Promise(() => {});
    });

    const initializer = configInitializer(configService)();

    expect(configService.init).toHaveBeenCalled();
  });
});
