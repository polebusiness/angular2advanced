import {ConfigService} from './config.service';

export function configInitializer(configService: ConfigService) {
  return () => configService.init();
}
