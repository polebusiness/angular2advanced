import {APP_INITIALIZER, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SecurityService} from './security/security.service';
import {AuthInterceptor} from './security/auth-interceptor';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {UnauthorizedInterceptor} from './security/unauthorized-interceptor';
import {RouterModule} from '@angular/router';
import {authInitializer} from './initializer/auth-initializer';
import {UserService} from '../account/shared/user.service';
import {ConfigService} from './config/config.service';
import {configInitializer} from './config/config-initializer';
import {AuthGuard} from './security/auth-guard.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [
    SecurityService,
    ConfigService,
    AuthGuard,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: UnauthorizedInterceptor, multi: true},
    {provide: APP_INITIALIZER, useFactory: authInitializer, deps: [SecurityService, UserService], multi: true},
    {provide: APP_INITIALIZER, useFactory: configInitializer, deps: [ConfigService], multi: true}
  ]
})
export class CoreModule {
}
