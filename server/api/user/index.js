const express = require('express');
const router = express.Router();
const userController = require('./user.controller');

router.get('/me', userController.meAction);
router.post('/authenticate', userController.authenticateAction);
router.get('/logout', userController.logoutAction);
router.post('/password', userController.passwordAction);
router.post('/create', userController.createAction);
router.get('/unauthorised', userController.unAuthorised);

module.exports = router;
