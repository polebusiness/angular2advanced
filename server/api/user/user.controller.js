module.exports = {
  authenticateAction,
  logoutAction,
  passwordAction,
  createAction,
  meAction,
  unAuthorised
};

const users = [{
  token: 'da39a3ee5e6b4b0d3255bfef95601890afd80709v',
  gender: 'Mr',
  firstName: 'John',
  lastName: 'Doe',
  email: 'j.doe@nowhere.com',
  password: 'test',
  addressInvoice: {
    street: '42 rue des tests',
    zipCode: '33000',
    city: 'Bordeaux',
    country: 'France'
  },
  addressesDelivery: [
    {
      street: '42 rue des tests',
      zipCode: '33000',
      city: 'Bordeaux',
      country: 'France',
      isDefault: true
    }
  ]
}];
let retry = true;
function authenticateAction(req, res, next) {
  console.log(retry);
  const selectedUser = users.find(user => user.email === req.body.email);
  if (selectedUser && (req.body.email === selectedUser.email && req.body.password === selectedUser.password) && !retry) {

    res.json({
      token: selectedUser.token
    });
  } else {
    res.status(400).end();
  }
  retry=!retry;
}

function createAction(req, res, next) {
  res.status(200).end();
}

function logoutAction(req, res, next) {
  res.status(200).end();
}

function passwordAction(req, res, next) {
  res.status(200).end();
}

function meAction(req, res, next) {
  const authHeader = req.get('authorization') ? req.get('authorization') : '';
  const authMatches = authHeader.match(/^Bearer\s(.+)/);
  const authToken = authMatches ? authMatches[1] : null;
  const selectedUser = users.find(user => user.token === authToken);
  if (selectedUser) {
    res.json(_sanitizeUser(selectedUser));
  } else {
    res.status(401).end();
  }
}

function _sanitizeUser(user) {
  user = {...user};
  user.token = undefined;
  user.password = undefined;
  return user;
}

function unAuthorised(req, res, next) {
  res.status(401).end();
}
