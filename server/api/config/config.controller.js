module.exports = {
  listAction
};

function listAction(req, res, next) {
  res.json({
    nbHighlightedProducts: 6,
    showCrossSells: true,
    sponsorshipEnabled: false
  });
}
