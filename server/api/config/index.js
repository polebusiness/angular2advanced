const express = require('express');
const router = express.Router();
const configController = require('./config.controller');

router.get('/', configController.listAction);

module.exports = router;
